# FlatParsing_Server

Script that parses InBerlinWohnen.de and hosts a webserver with basic info about the progress. The program scans the webpage inberlinwohnen.de for new flats, that don't require WBS, and then publishes them to telegram channel IBW_NoWBS, as well as printing the log file to a webserver.

##
The script parses the webpage InBerlinWohnen.de, all the output is printed in the log.txt. When a new flat has been found, than the script sends it over to a telegram channel InBerlinWohnen_NoWBS. 

The log file is getting cleaned up every now and then, and the list of objects saved in the program as well.

